import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity

# Загрузка данных из CSV файлов
df_output = pd.read_csv('final_output_file.csv')
with open('way.txt', 'r') as file:
    thr_file_path = file.readline().strip()
df_sample = pd.read_csv(thr_file_path)

# Создание векторизатора на основе текстов из обоих файлов
vectorizer = CountVectorizer()
corpus = df_output['Текст'].tolist() + df_sample['text'].tolist()
vectorizer.fit(corpus)

# Преобразование текстов в векторы
text_output_vectors = vectorizer.transform(df_output['Текст']).toarray()
text_sample_vectors = vectorizer.transform(df_sample['text']).toarray()

# Вычисление косинусной близости между текстами
similarity_matrix = cosine_similarity(text_output_vectors, text_sample_vectors)

# Создание списка для хранения словарей
data = []

for idx, row in df_output.iterrows():
    similarities = similarity_matrix[idx]
    max_similarity_idx = np.argmax(similarities)

    class_val = df_sample.at[max_similarity_idx, 'class']
    text_val = df_sample.at[max_similarity_idx, 'text']
    folder_val = row['Папка']
    similarity_val = similarities[max_similarity_idx]

    data.append({
        'class': class_val,
        'text': text_val,
        'Папка': folder_val,
        'similarity': similarity_val
    })

# Создание нового DataFrame из списка словарей
df_analyze = pd.DataFrame(data)

# Сохранение результатов в файл analyze.csv
df_analyze.to_csv('analyze.csv', index=False)
import subprocess

# Путь к исполняемому файлу Python
python_executable = "/usr/bin/python3"

# Путь к другому Python файлу, который вы хотите запустить
file_path = "Result2.py"

# Запускаем другой Python файл
subprocess.call([python_executable, file_path])
