import sys
from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QLabel, QPushButton, QFileDialog, QTextBrowser
import subprocess
import os

class FileManipulationApp(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Минималистичный интерфейс")
        self.setGeometry(100, 100, 400, 300)

        self.file_path_label = QLabel("Путь к файлу:")
        self.file_path_button = QPushButton("Выбрать файл")
        self.file_path_button.clicked.connect(self.choose_file)

        self.show_file_button = QPushButton("Показать содержимое файла")
        self.show_file_button.clicked.connect(self.show_file_content)

        self.execute_label = QLabel("Запустить bert.py:")
        self.execute_button = QPushButton("bert+tf-idf")
        self.execute_button.clicked.connect(self.execute_file)

        self.tf_idf_button = QPushButton("tf-idf")
        self.tf_idf_button.clicked.connect(self.execute_tf_idf)

        self.file_content = QTextBrowser()

        layout = QVBoxLayout()
        layout.addWidget(self.file_path_label)
        layout.addWidget(self.file_path_button)
        layout.addWidget(self.show_file_button)
        layout.addWidget(self.execute_label)
        layout.addWidget(self.execute_button)
        layout.addWidget(self.tf_idf_button)
        layout.addWidget(self.file_content)

        self.setLayout(layout)

        self.file_path = None  # Переменная для хранения пути к файлу

    def choose_file(self):
        file_dialog = QFileDialog()
        file_dialog.setFileMode(QFileDialog.ExistingFile)
        if file_dialog.exec_():
            file_paths = file_dialog.selectedFiles()
            self.file_path = file_paths[0]

    def show_file_content(self):
        if self.file_path:
            try:
                with open(self.file_path, 'r', encoding='utf-8') as file:
                    content = file.read()
                    self.file_content.setText(content)
            except FileNotFoundError:
                self.file_content.setText("Файл не найден.")
        else:
            self.file_content.setText("Выберите файл для отображения.")

    def execute_file(self):
        bert_script_path = 'bert.py'  # Укажите полный путь к bert.py

        if os.path.exists(bert_script_path):
            cmd = f'python3 {bert_script_path}'  # Используем python3 вместо python
            process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            output, error = process.communicate()
            if output:
                self.file_content.setText(output.decode())
            if error:
                self.file_content.setText(error.decode())
        else:
            self.file_content.setText("Файл bert.py не найден.")

    def execute_tf_idf(self):
        python_executable = "/usr/bin/python3"
        file_path = "tf-idf.py"
        subprocess.call([python_executable, file_path])
        self.file_content.setText("Файл tf-idf.py запущен.")

if __name__ == '__main__':
    app = QApplication(sys.argv)
    file_app = FileManipulationApp()
    file_app.show()
    sys.exit(app.exec_())
