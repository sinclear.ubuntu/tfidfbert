import csv

# Открываем CSV файл для чтения
with open('resultBERT.csv', 'r', newline='') as csvfile:
    csvreader = csv.reader(csvfile)
    
    # Ищем самое большое значение в третьем столбце
    max_value = float('-inf')
    max_row = []
    for row in csvreader:
        try:
            value = float(row[2])
            if value > max_value:
                max_value = value
                max_row = row
        except ValueError:
            pass

# Записываем найденную строку в текстовый файл
with open('output.txt', 'w') as txtfile:
    txtfile.write(','.join(max_row)) 
