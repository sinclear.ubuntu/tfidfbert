import pandas as pd
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import nltk
nltk.download('stopwords')
nltk.download('punkt')
import os
# Загрузка данных из CSV файла
df = pd.read_csv('output_table.csv')

# Создание списка русских стоп-слов
stop_words = set(stopwords.words('russian'))

# Группировка по папкам и объединение текстов по каждой папке
text_by_folder = df.groupby('Папка')['Текст'].apply(lambda x: ' '.join(x))

# Удаление стоп-слов из текста
def remove_stopwords(text):
    tokens = word_tokenize(text)
    filtered_text = [word for word in tokens if word.lower() not in stop_words]
    return ' '.join(filtered_text)

# Создание облака слов для каждой категории
for folder, text in text_by_folder.items():
    filtered_text = remove_stopwords(text)
    wordcloud = WordCloud(width=800, height=400, background_color='white').generate(filtered_text)
    plt.figure(figsize=(10, 6))
    plt.imshow(wordcloud, interpolation='bilinear')
    plt.axis('off')
    plt.title(f'Облако слов для "{folder}" (без стоп-слов)')
    plt.show()

    # Сохранение текста без стоп-слов в новый DataFrame
    df_filtered = pd.DataFrame({'Папка': [folder], 'Текст': [filtered_text]})

    # Добавление данных в новый CSV файл
    if not os.path.isfile('final_output_file.csv'):
        df_filtered.to_csv('final_output_file.csv', mode='w', index=False)
    else:
        df_filtered.to_csv('final_output_file.csv', mode='a', header=False, index=False)
