import os
import csv

# Путь к директории
directory_path = '/home/sa/Документы/dataset/'
selected_folders = ['Договоры', 'Заявления', 'Приказы', 'Соглашения', 'Уставы']

# Путь к CSV файлу
csv_file_path = 'output_table.csv'

# Открываем CSV файл для записи
with open(csv_file_path, 'w', newline='') as csvfile:
    csv_writer = csv.writer(csvfile)

    # Заголовки столбцов
    csv_writer.writerow(['Папка', 'Текст'])

    # Рекурсивно обходим выбранные папки
    for folder in selected_folders:
        folder_path = os.path.join(directory_path, folder)
        for root, _, files in os.walk(folder_path):
            for file in files:
                if file.endswith('.txt'):
                    file_path = os.path.join(root, file)

                    # Пишем название папки и текст из файла в CSV файл
                    csv_writer.writerow([folder, open(file_path).read()])

print(f'Таблица сохранена в файле: {csv_file_path}')
