import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from transformers import BertTokenizer, BertModel
import torch
from nltk.corpus import stopwords
import nltk
from wordcloud import WordCloud
import matplotlib.pyplot as plt
import subprocess

# Read the path to vullist file from way.txt
#with open('way.txt', 'r') as way_file:
vul_file_path = 'final_output_file.csv'

with open('way.txt', 'r') as file:
    thr_file_path = file.readline().strip()

nltk.download('stopwords')

# Check GPU availability
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
#device = ('cuda')
# Load data from CSV files
vullist_data = pd.read_csv(vul_file_path)
thrlist_data = pd.read_csv(thr_file_path)

# Define text columns for matching
vullist_text_columns = ['Папка', 'Текст']
thrlist_text_columns = ['class', 'text']

# Rest of your code remains the same
# Initialize TF-IDF vectorizer, load BERT models, compute similarity, create WordClouds, save results to a file, etc.

# Initialize TF-IDF vectorizer with Russian stopwords
russian_stop_words = stopwords.words('russian')
tfidf_vectorizer = TfidfVectorizer(stop_words=russian_stop_words)

# Load BERT tokenizer and model
tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
model = BertModel.from_pretrained('bert-base-uncased')
model.to(device)

# Function to get BERT embeddings
def get_bert_embeddings(text):
    inputs = tokenizer(text, return_tensors='pt', padding=True, truncation=True, max_length=512)
    with torch.no_grad():
        inputs = {key: val.to(device) for key, val in inputs.items()}
        outputs = model(**inputs)
    embeddings = torch.mean(outputs.last_hidden_state, dim=1)
    return embeddings

# Convert float values in columns to strings
vullist_data[vullist_text_columns] = vullist_data[vullist_text_columns].astype(str)
thrlist_data[thrlist_text_columns] = thrlist_data[thrlist_text_columns].astype(str)

# Get BERT embeddings for text data
vullist_texts = vullist_data[vullist_text_columns].apply(lambda x: ' '.join(x), axis=1)
vullist_text_embeddings = vullist_texts.apply(get_bert_embeddings)

thrlist_texts = thrlist_data[thrlist_text_columns].apply(lambda x: ' '.join(x), axis=1)
thrlist_text_embeddings = thrlist_texts.apply(get_bert_embeddings)

# Transform text data into TF-IDF vectors
vullist_tfidf = tfidf_vectorizer.fit_transform(vullist_texts)
thrlist_tfidf = tfidf_vectorizer.transform(thrlist_texts)

# Compute cosine similarity between TF-IDF vectors
similarity_matrix = cosine_similarity(vullist_tfidf, thrlist_tfidf)

# Create WordCloud for text in vullist
all_text_vullist = ' '.join(vullist_texts)
wordcloud_vullist = WordCloud(width=800, height=400, background_color='white').generate(all_text_vullist)

# Create WordCloud for text in thrlist
all_text_thrlist = ' '.join(thrlist_texts)
wordcloud_thrlist = WordCloud(width=800, height=400, background_color='white').generate(all_text_thrlist)

# Display WordClouds
plt.figure(figsize=(12, 6))
plt.subplot(1, 2, 1)
plt.imshow(wordcloud_vullist, interpolation='bilinear')
plt.title('WordCloud for output_table_stop.csv')
plt.axis('off')

plt.subplot(1, 2, 2)
plt.imshow(wordcloud_thrlist, interpolation='bilinear')
plt.title('WordCloud for sample.csv')
plt.axis('off')

plt.show()

# Save matching results to a file
result_file_path = '/home/sa/Документы/dataset/resultBERT.csv'
with open(result_file_path, 'w') as f:
    for i in range(len(vullist_data)):
        vullist_row = vullist_data.iloc[i]['Папка']
        most_similar_thrlist_row = thrlist_data.iloc[similarity_matrix[i].argmax()]['class']
        similarity_score = similarity_matrix[i].max()
        result = f'{vullist_row},{most_similar_thrlist_row},{similarity_score},\n'
        f.write(result)

print(f'Результаты сопоставления сохранены в файл {result_file_path}.')

import subprocess

import subprocess

# Путь к исполняемому файлу Python
python_executable = "/usr/bin/python3"

# Путь к другому Python файлу, который вы хотите запустить
file_path = "Result.py"

# Запускаем другой Python файл
subprocess.call([python_executable, file_path])
