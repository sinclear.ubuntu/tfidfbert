import pandas as pd
from gensim.models import Word2Vec
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from sklearn.metrics.pairwise import cosine_similarity

# Загрузка данных из файлов CSV
output_table_stop = pd.read_csv('output_table_stop.csv')
sample = pd.read_csv('sample.csv')

# Обработка данных, если нужно

# Обучение модели Word2Vec на столбце "Текст"
corpus = [text.split() for text in output_table_stop['Текст']]
w2v_model = Word2Vec(sentences=corpus, vector_size=100, window=5, min_count=1, sg=1)

# Векторизация текстов из столбцов "text" в таблице sample
sample['text_vector'] = sample['text'].apply(lambda x: w2v_model.infer_vector(x.split()))

# Нахождение близости между векторами
output_table_stop['closest_match'] = output_table_stop['Текст'].apply(lambda x: \
    max(sample.apply(lambda row: cosine_similarity([w2v_model.infer_vector(x.split())], [row['text_vector']]), axis=1))

# Сохранение результата в новую таблицу
output_table_stop.to_csv('output_with_similarity.csv', index=False)
