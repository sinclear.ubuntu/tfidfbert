from keras.models import Sequential
from keras.layers import Dense

# Создание последовательной модели
model = Sequential()

# Добавление слоев
model.add(Dense(units=64, activation='relu', input_dim=100))
model.add(Dense(units=10, activation='softmax'))

# Компиляция модели
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

# Вывод структуры модели
model.summary()
