import pandas as pd
import os

# Загрузка данных из CSV файла
df = pd.read_csv('123.csv')

# Проверка наличия второго столбца в данных
if len(df.columns) > 1:
    common_words = set()

    # Находим слова, которые присутствуют в каждой строке во втором столбце
    for idx, row in df.iterrows():
        words = set(row.iloc[1].lower().split())
        if not common_words:
            common_words = words
        else:
            common_words = common_words.intersection(words)

    # Удаляем только общие слова, которые присутствуют в каждой строке
    for idx, row in df.iterrows():
        words = set(row.iloc[1].lower().split())
        if common_words.issubset(words):
            df.at[idx, df.columns[1]] = ' '.join(list(words - common_words))

    # Сохраняем окончательный результат
    df.to_csv('final_output_file.csv', index=False)

else:
    print("Второго столбца в DataFrame не обнаружено")
